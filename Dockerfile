FROM igwn/lalsuite-dev:el7

LABEL name="LALSuite Development - CUDA"
LABEL maintainer="Adam Mercer <adam.mercer@ligo.org>"
LABEL support="Best Effort"

# add cuda repository
RUN yum-config-manager --add-repo http://developer.download.nvidia.com/compute/cuda/repos/rhel7/x86_64/cuda-rhel7.repo

# install cuda
RUN yum -y install nvidia-driver-latest-dkms cuda && \
    yum -y install cuda-drivers && \
    yum clean all
